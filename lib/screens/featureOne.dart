import 'package:flutter/material.dart';
import 'package:johnkonene/screens/profile.dart';

class FeatureOne extends StatelessWidget {
  const FeatureOne({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold (
            appBar: AppBar(
            centerTitle: true,
            title: const Text("Feauter One")
            ),

            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Feature One',
                  style: TextStyle(fontSize: 50),),
                ],
              ),
          ),
        
          floatingActionButton: FloatingActionButton(
            onPressed: () {

                Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>const Profile()));

            },
            child: const Icon(Icons.add),
         ),
    );
  }
}
